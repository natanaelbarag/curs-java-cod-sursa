package com.primulProiect.bancomat;

import java.util.Scanner;

public class AplicatieBancomat {
    public static void main(String[] args) {
        Scanner citotr = new Scanner(System.in);
        int sold = 1000;
        int pinCorect = 3452;

        System.out.println("Bun venit!");
        System.out.println("Introduceti username: ");
        String username = citotr.nextLine();
        System.out.println("Introduceti pin: ");
        int pin = citotr.nextInt();
        if (username.equals("naebara") && pin == pinCorect) {
            System.out.println("Introduceti suma pentru retragere: ");
            int suma = citotr.nextInt();
            while (suma > sold) {
                System.out.println("Sold insuficient!");
                System.out.println("1. Introduceti din nou suma");
                System.out.println("2. Reuntati");
                int optiune = citotr.nextInt();
                if (optiune == 1) {
                    System.out.println("Introduceti suma pentru retragere: ");
                    suma = citotr.nextInt();
                } else {
                    break;
                }
            }
            if (suma <= sold) {
                sold = sold - suma;
                System.out.println("Suma retrasa : " + suma);
                System.out.println("Suma disponibila: " + sold);
            }

        } else {
            System.out.println("Date de login incorecte! Incercati din nou!");
        }

        System.out.println("La revedere");
    }
}
