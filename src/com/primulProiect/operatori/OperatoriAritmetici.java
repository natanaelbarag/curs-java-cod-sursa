package com.primulProiect.operatori;

public class OperatoriAritmetici {
    public static void main(String[] args) {
        int a = 5;
        float b = 2;

        // +
        System.out.println("a + b = " + (a + b));
        // -
        System.out.println("a - b = " + (a - b));
        // *
        System.out.println("a * b = " + (a * b));
        // /
        System.out.println("a / b = " + (a / b));

        // %
        System.out.println(10 % 3);

        // 1 - ultima cifra a numarului
        //%10
        int c = 236773;
        System.out.println(c % 10);
        int ultimaCifra = c % 10;
        // 2 - numarul fara ultima cifra
        // /10
        System.out.println(c / 10);
        int numarFaraUC = c / 10;
    }
}
