package com.primulProiect.operatori;

public class OperatoriRelationali {
    public static void main(String[] args) {
        int a = 4;
        int b = 7;

        // ==
        System.out.println(a == b);
        // !=
        System.out.println(a != b);
        // <
        System.out.println(a < b);
        // >
        System.out.println(a > b);
        // >=
        System.out.println(a >= b);
        // <=
        System.out.println(a <= b);

        boolean egalitate = a == b;
    }
}
