package com.primulProiect.operatori;

public class OperatoriLogici {
    public static void main(String[] args) {
        int nota = 6;
        int grade = 29;

        // &&
        System.out.println((nota >= 8) && (grade > 25));

        // ||
        System.out.println(nota >= 8 || grade > 25);

        // !
        System.out.println(!true);

        System.out.println("---------------");
        System.out.println(5 > 3 && 8 > 5); // true
        System.out.println(5 > 3 && 8 < 5); // false

        System.out.println(5 < 3 || 8 > 5); // true
        System.out.println(5 > 3 || 8 < 5); // true

        System.out.println(!(5 == 3));
        System.out.println(!(5 > 3));
    }
}
