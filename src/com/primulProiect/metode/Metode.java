package com.primulProiect.metode;

public class Metode {
    public static void main(String[] args) {
        //
        int a = 465;
        int cifreA = cateCifre(a);
        System.out.println(cifreA);

        int b = 25326;
        int cifreB = cateCifre(b);
        System.out.println(cifreB);


    }

    static int cateCifre(int nr) {
        int c = 0;
        while (nr > 0) {
            c++;
            nr = nr / 10;
        }
        return c;
    }
}
