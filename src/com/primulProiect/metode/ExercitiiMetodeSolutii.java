package com.primulProiect.metode;

public class ExercitiiMetodeSolutii {
    public static void main(String[] args) {
        String cuvinte = "merg in parc";
        int nrCuvinte = cateCuvinte(cuvinte);
        System.out.println("'" + cuvinte + "' are " + nrCuvinte + " cuvinte");
    }

    static int cateCuvinte(String s) {
        int contor = 0;
        for (int i = 0; i < s.length(); i++) {
            if (s.charAt(i) == ' ') {
                contor++;
            }
        }
        return contor + 1;
    }


}
