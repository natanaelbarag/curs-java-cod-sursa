package com.primulProiect.control;

public class InstructiuneaSwitch {
    public static void main(String[] args) {

        int zi = -3;
        String numeZi;
        switch (zi) {
            case 1:
                numeZi = "luni";
                break;
            case 2:
                numeZi = "marti";
                break;
            case 3:
                numeZi = "miercuri";
                break;
            case 4:
                numeZi = "joi";
                break;
            case 5:
                numeZi = "vineri";
                break;
            case 6:
                numeZi = "sambata";
                break;
            case 7:
                numeZi = "duminica";
                break;
            default:
                numeZi = "Valoare invalida";
        }
        System.out.println(numeZi);

    }
}
