package com.primulProiect.control;

public class InstructiuneaWhile {
    public static void main(String[] args) {
        int a = 2563;

        while (a > 0) {
            System.out.println(a % 10); // ultima cifra
            a = a / 10; // numarul fara ultima cifra
        }

        // suma cifrelor unui numar
        int n = 12;
        int suma = 0;
        while (n > 0) {
            int uc = n % 10;
            suma = suma + uc;
            n = n / 10;
        }
        System.out.println("Suma cifrelor este :" + suma);

    }
}
