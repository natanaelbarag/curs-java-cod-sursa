package com.primulProiect.control;

public class InstructiuneaIfElse {
    public static void main(String[] args) {
        int a = -4;
        if (a >= 0) {
            System.out.println("Numarul este pozitiv");
        } else {
            System.out.println("Numarul este negativ");
        }

        String nume = "na";
        if (nume.length() >= 3) {
            System.out.println("Cuvantul " + nume + " are cel putin 3 caractere");
        } else {
            System.out.println("Cuvantul " + nume + " NU are cel putin 3 caractere");
        }
    }
}
