package com.primulProiect.tipuriDeDate;

import java.util.Scanner;

public class CitireDeLaTastatura {

    public static void main(String[] args) {
        Scanner cititor = new Scanner(System.in);

        System.out.println("Introdu numele tau: ");
        String nume = cititor.nextLine();

        System.out.println("Introdu varsta ta: ");
        int varsta = cititor.nextInt();

        System.out.println("Nume: " + nume);
        System.out.println("Varsta: " + varsta);


    }
}
