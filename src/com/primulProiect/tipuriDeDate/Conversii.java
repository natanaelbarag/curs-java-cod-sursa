package com.primulProiect.tipuriDeDate;

public class Conversii {
    public static void main(String[] args) {
        byte a = 5;
        int b = a;

        int c = 174;
        short d = (short) c;

        System.out.println("From byte to int " + b);
        System.out.println("From int to short " + d);

        double pretPaine = 2.34;
        int pretPaineToInt = (int) pretPaine;
        System.out.println(pretPaineToInt);

        short x = 56;
        double t = x;
        System.out.println(t);

        int u = 123;
        String u1 = String.valueOf(u);
        System.out.println(u1);

        String d1 = "12d4";
        int d2 = Integer.parseInt(d1);
        System.out.println(d2);
    }
}
