package com.primulProiect.tipuriDeDate;

import java.util.Scanner;

public class Suma2Numere {
    public static void main(String[] args) {
        Scanner cititor = new Scanner(System.in);
        System.out.println("Introdu primul numar: ");
        int nr1 = cititor.nextInt();

        System.out.println("Introdu al doilea numar: ");
        int nr2 = cititor.nextInt();

        System.out.println("Suma este " + (nr1 + nr2));
        System.out.println("Diferenta este " + (nr1 - nr2));
        System.out.println("Impartirea este " + (nr1 / nr2));
        System.out.println("Inmultirea este " + (nr1 * nr2));
    }
}
