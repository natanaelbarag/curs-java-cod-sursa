package com.primulProiect.exempleRezolvate;

public class StringPalindrom {
    public static void main(String[] args) {
        String word = "Masa";
        String og = "";
        for (int i = 0; i < word.length(); i++) {
            og = word.charAt(i) + og;
        }
        System.out.println(og);
        if (og.equals(word)) {
            System.out.println("Cuvantul este palindrom");
        } else {
            System.out.println("Cuvantul nu este palindrom");
        }
    }
}
