package com.primulProiect.exempleRezolvate;

public class NumarPalindrom {
    public static void main(String[] args) {
        int a = 121;
        int o = 0;
        int copy = a;
        while (a > 0) {
            int c = a % 10;
            o = o * 10 + c;
            a = a / 10;
        }

        if (o == copy) {
            System.out.println("Palindrom");
        } else {
            System.out.println("Nu este Palindrom");
        }
    }
}
