package com.primulProiect.exempleRezolvate;

public class CeaMaiMareCifra {
    public static void main(String[] args) {
        // Exemple rezolvate
        int a = 32946346;
        int x = a % 10;

        while (a > 0) {
            if (a % 10 > x) {
                x = a % 10;
            }
            a = a / 10;
        }
        System.out.println(x);
    }
}
