package com.primulProiect.exempleRezolvate;

public class NumarPrim {
    public static void main(String[] args) {
        int a = 21;
        int c = 0;
        for (int i = 1; i <= a; i++) {
            if (a % i == 0) {
                c++;
            }
        }
        if (c == 2) {
            System.out.println("Este prim");
        } else {
            System.out.println("Nu este prim");
        }
    }
}
