package com.primulProiect.liste;

import java.util.Arrays;

public class Liste {
    public static void main(String[] args) {
//        type varName[] = value;

        int[] note = new int[10];
        int[] numere = {3, 5, 4, 2, 8};
        System.out.println("primul numar : " + numere[0]);
        System.out.println("al doilea numar : " + numere[1]);
        System.out.println("al treilea numar : " + numere[2]);
        System.out.println("al patrulea numar : " + numere[3]);
        System.out.println("al cincilea numar : " + numere[4]);
        System.out.println(numere.length);
        System.out.println(Arrays.toString(numere));
        System.out.println(Arrays.toString(note));


        // array[index]
//        System.out.println(numere[4]);
//
//        System.out.println(note[0]);
//        note[0] = 88;
//        System.out.println(note[0]);

    }
}
